var schoolDeliveryPresenter = (function () {

    function init() {
        $.when(//when all requests..
            $.get('school-delivery/dish-list'),
            $.get('school-delivery/child-list'),
            $.get('school-delivery/week-list')
        ).done(//.. are done, do this with their data...
            function (dishList, childList, weekList) {// Each argument is an array with the following structure: [ data, statusText, jqXHR ]
                //first init app state
                appState.allDishes = JSON.parse(dishList[0]);
                appState.allWeeks = JSON.parse(weekList[0]);
                appState.allChildren = JSON.parse(childList[0]);
                appState.selectedChildIndex = 0;
                appState.selectedWeekIndex = 0;
                appState.selectedDishes = {};
                //then update views..
                updateDishList(appState.allDishes);
                handleGetChildList(childList[0]);
                handleGetWeekList(weekList[0]);
                bindPayButtonClick();
                bindNextWeekButtonClick();
            }
        );
    }

    function updateDishList(dishList) {
        var dishListSelector = $('#dish-list');
        dishListSelector.html('');

        Object.keys(dishList).forEach(function (dishGroupName, index) {
            dishListSelector.append(
                '<div class="clearfix"></div>' +
                '<div class="dish-group"><h1 class="dish-group-heading-1">' + dishGroupName + '</h1>');
            var dishesInCategory = dishList[dishGroupName];
            dishesInCategory.forEach(function (dish, index) {

                var itemId = 'dish-id-' + dish.id;
                var currentDish = $('#example-dish-view')
                    .clone()
                    .attr({'id': itemId, 'data-dish-id': dish.id})
                    .appendTo('#dish-list')
                    .show();


                if (typeof appState.selectedDishes[appState.selectedChildIndex] !== 'undefined') {
                    appState.allDays.forEach(function (val, idx) {
                        var key = getSelectedDishKey(dish.id, val);
                        if (key in appState.selectedDishes[appState.selectedChildIndex]) {
                            $(currentDish)
                                .find('.dish-' + val + '-button').toggleClass('dish-day-selected');
                        }
                    });
                }
                var dishElement = document.getElementById(itemId);
                //add data to dish
                if (dish.picture) {
                    $(dishElement).find('.dish-picture > img').attr('src', 'img/dishes/' + dish.picture);
                }
                else {
                    $(dishElement).find('.dish-picture > img').attr('src', 'img/default.png');
                }
                $(dishElement).find('.dish-name-text').html(dish.name);
                $(dishElement).find('.dish-price-text').html(parseInt(dish.price) + ',-');
                $(dishElement).find('.dish-description-text').html(dish.description);

                if (dish.non_vegetarian == 1) {
                    $(dishElement).find('.dish-non-vegetarian').show();
                }
                if (dish.contains_milk == 1) {
                    $(dishElement).find('.dish-contains-milk').show();
                }
                if (dish.contains_gluten == 1) {
                    $(dishElement).find('.dish-contains-gluten').show();
                }


                //add click event for each button
                $(dishElement).find('.dish-monday-button').click(function () {
                    var dishId = $(this).parent().parent().attr('data-dish-id');
                    toggleDishSelect(dishId, 'monday');
                    $(this).toggleClass('dish-day-selected');
                });
                $(dishElement).find('.dish-tuesday-button').click(function () {
                    var dishId = $(this).parent().parent().attr('data-dish-id');
                    toggleDishSelect(dishId, 'tuesday');
                    $(this).toggleClass('dish-day-selected');
                });
                $(dishElement).find('.dish-wednesday-button').click(function () {
                    var dishId = $(this).parent().parent().attr('data-dish-id');
                    toggleDishSelect(dishId, 'wednesday');
                    $(this).toggleClass('dish-day-selected');
                });
                $(dishElement).find('.dish-thursday-button').click(function () {
                    var dishId = $(this).parent().parent().attr('data-dish-id');
                    toggleDishSelect(dishId, 'thursday');
                    $(this).toggleClass('dish-day-selected');
                });
                $(dishElement).find('.dish-friday-button').click(function () {
                    var dishId = $(this).parent().parent().attr('data-dish-id');
                    toggleDishSelect(dishId, 'friday');
                    $(this).toggleClass('dish-day-selected');
                });
            });
            dishListSelector.append('</div>');
        });
    }

    function toggleDishSelect(dishId, day) {

        var key = getSelectedDishKey(dishId, day);
        if (typeof appState.selectedDishes[appState.selectedChildIndex] === 'undefined') {
            appState.selectedDishes[appState.selectedChildIndex] = {};
        }

        if (!(key in appState.selectedDishes[appState.selectedChildIndex])) {
            appState.selectedDishes[appState.selectedChildIndex][key] = 1;
        } else {
            delete appState.selectedDishes[appState.selectedChildIndex][key];
        }

        updateSidebarDishlist();
    }

    function getSelectedDishKey(dishId, day) {
        return '' + dishId + ';' + day + ';' + appState.allWeeks[appState.selectedWeekIndex];
    }

    function handleGetChildList() {
        //show first child...
        var childSelectHtml = '';
        appState.allChildren.forEach(function (childObj, index) {
            childSelectHtml += '<option value="' + index + '">' + childObj.name + '</option>';
        });
        $('#child-select').html(childSelectHtml);
        var childSelectElement = document.getElementById('child-select');
        $(childSelectElement).change(function () {
            var childIndex = $(childSelectElement).val();
            selectChild(childIndex);
        });
        selectChild(0);
    }

    function selectChild(childIndex) {
        var child = appState.allChildren[childIndex];
        $('#child-select').val(childIndex);
        appState.selectedChildIndex = childIndex;
        if (child.picture) {
            $('#child-picture').attr('src', 'img/children/' + child.picture);
        } else {
            $('#child-picture').attr('src', 'img/default_child.png');
        }
        $('#child-name').html(child.name);
        updateDishList(appState.allDishes);
        updateSidebarDishlist();
    }

    function handleGetWeekList(data) {
        appState.allWeeks = JSON.parse(data);

        var weekSelectHtml = '';

        Object.keys(appState.allWeeks).forEach(function (key) {
            weekSelectHtml += '<option value="' + key + '">' + appState.allWeeks[key] + '</option>';
        });

        $('#week-select').html(weekSelectHtml);

        var weekSelectElement = document.getElementById('week-select');
        $(weekSelectElement).change(function () {

            var weekIndex = $(weekSelectElement).val();
            selectWeek(weekIndex);
        });
        selectWeek(0);
    }

    function selectWeek(weekIndex) {
        var weekSelectElement = document.getElementById('week-select');
        appState.selectedWeekIndex = weekIndex;
        $(weekSelectElement).val(weekIndex);
        updateDishList(appState.allDishes);
        updateSidebarDishlist();
    }

    function bindPayButtonClick() {
        $("#pay-button").click(handlePayButtonClick);
    }

    function bindNextWeekButtonClick() {
        $('#next-week-button').click(handleNextWeekButtonClick);
    }

    function handleNextWeekButtonClick() {

        //increase week index (and select the week) until you reach the max possible index.
        var maxWeekIndex = 0;
        Object.keys(appState.allWeeks).forEach(function (val, key) {
            maxWeekIndex = key;
        });


        var newIndex = parseInt(appState.selectedWeekIndex) + 1;
        if (newIndex <= parseInt(maxWeekIndex)) {
            appState.selectedWeekIndex = newIndex
        }


        selectWeek(appState.selectedWeekIndex);
    }


    function handlePayButtonClick() {
        var sendData = [];
        Object.keys(appState.selectedDishes).forEach(function (childIndex) {
            sendData[appState.allChildren[childIndex].id] = appState.selectedDishes[childIndex];
        });

        $.post('school-delivery/pay',
            {
                selectedDishes: sendData
            },
            function (data) {
                if (data != 'no_data' && parseInt(data) > 0) {//the original controller would calculate and return the price of the order...

                    var orderId = parseInt(data);
                    //here original app calls ePay window...
                    console.log("You clicked [Pay]. Sending data to the payment controller...");
                    console.log(sendData);


                } else {
                    alert('No dishes selected');
                }
            }
        );
    }

    function updateSidebarDishlist() {
        var currentWeek = appState.allWeeks[appState.selectedWeekIndex];
        var dishesToSee = {'monday': {}, 'tuesday': {}, 'wednesday': {}, 'thursday': {}, 'friday': {}};

        //if selected dishes for this kid even exist...
        if (appState.selectedDishes.hasOwnProperty(appState.selectedChildIndex)) {

            //loop and add them to dishesToSee
            Object.keys(appState.selectedDishes[appState.selectedChildIndex]).forEach(function (mealKeyValue) {
                var valArr = mealKeyValue.split(';');
                var dishId = valArr[0];
                var dayStr = valArr[1];
                var week = valArr[2];
                if (week == currentWeek) {//of the week is the same
                    //put the meal in the list
                    dishesToSee[dayStr][dishId] = mealKeyValue;
                }
            });

        }

        //generate the html
        var days = appState.allDays;
        var max = days.length;
        var dishHTML = '';
        for (var i = 0; i < max; i++) {

            var day = days[i];

            dishHTML += '<div class="checkout-day">' + day.charAt(0).toUpperCase() + day.slice(1) + '</div>';//capitalize first letter
            dishHTML += '<ul>';

            Object.keys(dishesToSee[day]).forEach(function (dVal) {
                var dishArr = dVal.split(';');
                var liDish = getDishById(dishArr[0]);
                dishHTML += '<li onclick="schoolDeliveryPresenter.removeDishFromSidebar(\'' + getSelectedDishKey(dishArr[0], day) + '\')">'
                    + liDish.name + '<span class="remove"><i class="fa fa-times" aria-hidden="true"></i></span>' +
                    '</li>';
            });

            dishHTML += '</ul>';
        }
        jQuery('#all-selected-dishes').html(dishHTML);
        jQuery('#order-total-amount').html(getOrderTotal());
    }

    function handleRemoveDishFromSidebar(selectedDishKey) {
        var dishArray = selectedDishKey.split(';');
        toggleDishSelect(dishArray[0], dishArray[1]);
        updateDishList(appState.allDishes);
    }

    function getDishById(dishId) {
        var dishList = appState.allDishes;
        var dish;

        //iterate over all dishes and return the one
        Object.keys(dishList).forEach(function (dishGroupName, index) {
            var dishesInCategory = dishList[dishGroupName];
            dishesInCategory.forEach(function (cDish, idx) {
                if (cDish.id == dishId) {
                    dish = cDish;
                }
            });
        });

        if (typeof dish === 'undefined') {
            return false;
        } else {
            return dish;
        }
    }

    function getOrderTotal() {
        var price = 0;
        Object.keys(appState.selectedDishes).forEach(function (childIndex) {
            Object.keys(appState.selectedDishes[childIndex]).forEach(function (val, key) {
                var selectRow = val.split(';');
                var dishId = selectRow[0];
                var dish = getDishById(dishId);
                price += parseInt(dish.price);
            });
        });

        return price;
    }

    return {
        init: init,
        removeDishFromSidebar: handleRemoveDishFromSidebar,
        getOrderTotal: getOrderTotal
    }
})();